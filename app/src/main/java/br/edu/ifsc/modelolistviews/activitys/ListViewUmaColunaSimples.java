package br.edu.ifsc.modelolistviews.activitys;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.edu.ifsc.modelolistviews.R;
import br.edu.ifsc.modelolistviews.controler.Frutas;
import br.edu.ifsc.modelolistviews.models.Fruta;

public class ListViewUmaColunaSimples extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uma_coluna_simples);
        //Associando a variavel local listView a o objeto instaciado no layout com id listView
        ListView listView = findViewById(R.id.listView);

        //Carregando Dados predefinidos chamando o controlador Frutas e solicitando a lista de frutas
        Frutas frutas= new Frutas();

        //Adiquirindo lista de frutas em formato String que serão apresentados no listView
        ArrayList<String> listaNomeFrutas = new ArrayList<>();
        for (Fruta f: frutas.FRUTAS) {
            listaNomeFrutas.add(f.getNome());
        }

        /**
         * Carregando um adapter com a estrutura das informações que configuram o ListView
         * O Adapter tem a responsabidade de instanciar o layout definido para cada linha,
         * para o número de dados passado no arrayList,
         * além disso ele o adapter atribui cada registro de dados os campos informados que estão no layout.
         *          *
         * 1º parametro - Contexto da classe atual
         * 2º parametro - Definição de um layout, que tem um textview dentro, segure o control e clique com botão direito e veja o xml
         * 3º parametro - id do textView definido no layout passado no layout a ser exibido
         * 4º parametro - array com dados do mesmo tipo informadao no adapter
         * */
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                listaNomeFrutas
        );


        //Solicita ao listView que carregue o layout e dados passados no arrayAdapter e apresente
        listView.setAdapter(arrayAdapter);
    }






}

