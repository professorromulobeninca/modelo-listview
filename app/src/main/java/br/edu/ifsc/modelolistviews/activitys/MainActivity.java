package br.edu.ifsc.modelolistviews.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import br.edu.ifsc.modelolistviews.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void abreActivityUmaColuna(View v){
        startActivity(new Intent(this,ListViewUmaColunaSimples.class));
    }
    public void abreActivityDuasLinhas(View v){
        startActivity(new Intent(this,ListViewDuasLinhasSimples.class));
    }
    public void abreActivityDuasColunas(View v){
        startActivity(new Intent(this,ListViewDuasColunasSimples.class));
    }
    public void abreActivityTresColunas(View v){
        startActivity(new Intent(this,ListViewTresColunasSimples.class));
    }

    public void abreActivityComListViewPersonalizada(View v){
        startActivity(new Intent(this,ListViewPersonalizada.class));
    }

}
